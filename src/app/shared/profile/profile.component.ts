import { Component, OnInit, Input } from '@angular/core';
import { Observable, BehaviorSubject, pipe, interval } from "rxjs";
import * as geofirex from "geofirex";
import * as firebaseApp from "firebase/app";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  geo = geofirex.init(firebaseApp);
  @Input()
  user;
  collection: any;
  testDoc;
  interval;
  docId;
  constructor() {}

  ngOnInit() {
    this.collection = this.geo.collection("positions");
  }
  
      createPoint1() {
    this.interval = setInterval(()=>{
    let lat = 41.40673625596859 + this.rand();
    let lng = 2.1667253466434886 + this.rand();
    this.docId = this.user.uid;
    const point = this.geo.point(lat, lng);
    const data = { nombre: this.user.displayName, docId: this.docId, position: point.data, allow: true, online: true};
    this.collection.setDoc(this.docId, data);
	console.log(point);
	console.log(data); },2000); 
  }
    rand() {
    const arr = [0.01, -0.01];
    return arr[Math.floor(Math.random() * arr.length)];
  }
  
    stopgeo() {
    clearInterval(this.interval);
    }
  
}
